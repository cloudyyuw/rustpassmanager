pub struct Pass {
    pub id: i32,
    pub key: String,
    pub pass: String
}

pub struct PartPass {
    pub id: i32,
    pub key: String
}
