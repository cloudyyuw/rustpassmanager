use crate::{Pass, PartPass};
use rusqlite::Connection;

pub fn delete_key(filepath: String, key: String) -> rusqlite::Result<()> {
    let con = Connection::open(filepath)?;

    con.execute("
        DELETE FROM pass 
        WHERE key = ?1
    ", [key])?;

    Ok(())
}

pub fn update_key_value(filepath: String, key: String, content: String) -> rusqlite::Result<()> {
    let con = Connection::open(filepath)?;

    con.execute("
        UPDATE pass 
        SET pass = ?2 
        WHERE key = ?1
    ", ( key, content ))?;

    Ok(())
}

pub fn list_keys(filepath: String) -> rusqlite::Result<Vec<PartPass>> {
    let con = Connection::open(filepath)?;

    let mut query = con.prepare("
        SELECT id,key FROM pass
    ")?;

    query.query_map([], |row| {
        Ok(PartPass {
            id: row.get(0)?,
            key: row.get(1)?
        })
    }).and_then(Iterator::collect)
}

pub fn fetch_key(filepath: String, key: String) -> rusqlite::Result<Vec<Pass>> {
    let con = Connection::open(filepath)?;

    let mut query = con.prepare("
        SELECT * FROM pass
        WHERE key = ?1
    ")?;

    query.query_map([key], |row| {
        Ok(Pass {
            id: row.get(0)?,
            key: row.get(1)?,
            pass: row.get(2)?
        })
    }).and_then(Iterator::collect)
}

pub fn insert_key(filepath: String, key: String, content: String) -> rusqlite::Result<()> {
    let con = Connection::open(filepath)?;

    con.execute("
        INSERT INTO pass (key, pass)
        VALUES (?1, ?2)
    ", ( key, content ))?;

    Ok(())
}

pub fn setup(filepath: String, gpg: bool, fingerprint: String) -> rusqlite::Result<()> {
    let con = Connection::open(filepath)?;

    con.execute("
        CREATE TABLE IF NOT EXISTS config (
            gpg boolean default false,
            fingerprint varchar(255) default \"\"
        )
    ", [])?;
    con.execute("
        CREATE TABLE IF NOT EXISTS pass (
            id integer primary key autoincrement,
            key varchar(255) not null,
            pass mediumtext not null
        )
    ", [])?;

    con.execute("DELETE FROM config", ())?;

    con.execute("
        INSERT INTO config (gpg, fingerprint)
        VALUES (?1, ?2)
    ", (gpg, fingerprint))?;

    Ok(())
}
