use std::env;
use std::io;
use std::path::Path;
use std::fs;

mod db;
mod types;

use types::{Pass, PartPass};

fn main() {
    let args: Vec<String> = env::args().collect(); 
    let param = &args[1];
    let config_dir =  dirs::config_dir()
        .unwrap()
        .into_os_string()
        .into_string()
        .unwrap();
    let filepath = format!("{}/rustpassmanager/pass.db", config_dir);

    if param == "--setup" {
        let mut res = String::new();
        
        println!("Do you want to use GPG? [y|n] ");
        io::stdin()
            .read_line(&mut res)
            .expect("Enter an valid response");
        if let Some('\n') = res.chars().next_back(){ res.pop(); }
        if let Some('\r') = res.chars().next_back(){ res.pop(); }

        // check if $HOME/.config/rustpassmanager exists
        if !Path::new(&format!("{}/rustpassmanager", config_dir)).exists(){
            fs::create_dir_all(format!("{}/rustpassmanager", config_dir));
        }

        if res == "y" {
            let mut finprt = String::new();

            println!("Enter an GPG fingerprint or email: ");
            io::stdin()
                .read_line(&mut finprt)
                .expect("Enter an valid fingerprint");
            if let Some('\n') = finprt.chars().next_back(){ finprt.pop(); }
            if let Some('\r') = finprt.chars().next_back(){ finprt.pop(); }

            db::setup(filepath.clone(), true, finprt);

            println!("Done!")
        } else {
            db::setup(filepath.clone(), false, "".to_owned());
            println!("Done!")
        }
        return
    }
    if param == "-a" || param == "-add" {
        let key_name = &args[2];
        let mut content = String::new();

        println!("Content: [paste or type] ");

        io::stdin()
            .read_line(&mut content)
            .expect("Enter an valid content");

        if content.is_empty() {
            println!("Enter an valid content");
            return
        }

        db::insert_key(filepath.clone(), key_name.to_owned(), content);
        
        return
    }

    if param == "-g" || param == "-get" { 
        let varname = &args[2];
        
        match db::fetch_key(filepath.clone(), varname.to_owned()) {
            Ok(keys) => println!("[ID: #{}] {}:\n{}", &keys[0].id, &keys[0].key, &keys[0].pass),
            Err(_)    => println!("Empty")
        }
        return
    }
    
    if param == "-l" || param == "-list" {
        let keys = match db::list_keys(filepath.clone()) {
            Ok(keys) => keys,
            Err(err) => panic!("Err: {:?}", err),
        };

        for key in keys {
            println!("[ID: #{}] {}", key.id, key.key)
        }

        return
    }

    if param == "-u" || param == "-update" {
        let varname = &args[2];
        let mut new_content = String::new();

        println!("Insert a new content: ");

        io::stdin()
            .read_line(&mut new_content)
            .expect("Enter an valid content");

        match db::update_key_value(filepath.clone(), varname.to_owned(), new_content.to_owned()) {
            Ok(_)  => println!("Done!"),
            Err(e) => panic!("Err: {:?}", e)
        }
    }

    if param == "-d" || param == "-delete" {
        let varname = &args[2];

        match db::delete_key(filepath.clone(), varname.to_owned()) {
            Ok(_)  => println!("Done!"),
            Err(e) => panic!("Err: {:?}", e)
        }

        return
    }

    if param == "-h" || param == "-help" {
        println!("Arguments:\nAdd:    -a | -add <name>\nGet:    -g | -get <name>\nList:   -l | -list\nUpdate: -u | -update <name>\nDelete: -d | -delete <name>")
    }
    else { 
        println!("Invalid argument.\nUse -h | -help for the argument list.")
    }
}
