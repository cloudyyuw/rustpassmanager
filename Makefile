BINPATH=${HOME}/.local/bin
TARGET=x86_64-unknown-linux-gnu
OUTPATH=${PWD}/target/${TARGET}/release/passmanager

list-targets:
	rustup target list 

default-target:
	rustup show

install-rustup:
	curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

build:
	cargo build --target ${TARGET} --release

install:
	mv ${OUTPATH} ${BINPATH}/rspass
	chmod +x ${BINPATH}/rspass
