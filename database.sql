CREATE TABLE IF NOT EXISTS config (
  gpg boolean default false,
  fingerprint varchar(255) default ""
)

CREATE TABLE IF NOT EXISTS pass (
  id integer primary key autoincrement,
  key varchar(255) not null,
  pass mediumtext not null
)
